# Environment Setup

This document provides pointers on how to set up your development environment to run and work on the demos and exercises presented in this Mobile Robotics Crash Course.


## Operating System

The main piece of software we use is the [Robot Operating Sytem (ROS) 2](www.ros2.org). While ROS 2 is technically supported on Windows 10 and to a lesser extend on MacOS, Ubuntu has the most polished experience, largely because it has been the operating system of choice since the inception of ROS in 2007. Therefore, we strongly recommend that you install ROS 2 on Ubuntu and it is the OS we will assume you will be using throughout this Crash Course.

> Note that the latest version of Ubuntu is 22.04. The resources in this Crash Course have only been tested on **Ubuntu 20.04**, hence going through this Crash Course on newer versions of Ubuntu **may not work**.

This section outlines a number of ways to get access to Ubuntu on your machine. Some will be better than others in terms of difficulty of setup, performance and reliability.

### Dual Boot

Dual booting your machine is the most permanent of the options presented in this section. This involves partitioning your hard drive and installing Ubuntu 20.04 into the partition using a boot-able USB drive.

Pros:
* Best performance - operating system runs directly on your computer hardware
* Support - we have plenty of experience doing this at EUFS and so can help you do this

Cons:
* Quite a permanent solution (Can be difficult to re-size partitions)
* There are risks associated, backing up your computer is certainly recommended

To dual boot, you must first create a boot-able USB drive containing Ubuntu 20.04, boot into the USB drive, and install Ubuntu from there. Conveniently, there is [documentation detailing the entire process](https://ubuntu.com/tutorials/install-ubuntu-desktop#1-overview) from Canonical (the company behind Ubuntu).

### Hyper-V (Windows 10 and 11)

Windows has a built in system which makes creating and running virtual machines very easy. This system is called Hyper-V. Unlike VirtualBox (see below), it is a Type 1 virtual machine, which exposes the guest OS to your computer’s hardware, rather than a type 2 virtual machine which has an extra "middle man" layer of software. This makes Type 1 virtual machines better in terms of performance and so we recommend using Hyper-V over VirtualBox if possible.

Pros:
* Simple to set up
* Low risk
* Temporary - you can easily delete everything once you’re done

Cons:
* Slight performance penalty - you are running a virtual machine
* Windows specific

To use Ubuntu with Hyper-V follow [this tutorial](https://dellwindowsreinstallationguide.com/ubuntu-20-04-lts-hyper-v/). The ability to maximise the guest OS window is not strictly necessary, but it is nice to have. We would also recommend giving your VM control of a significant proportion of your RAM and CPU, as this will increase performance. If using this, ensure that you aren’t simultaneously running any other resource intensive applications.

### WSL 2

Windows has an additional feature called Windows Subsystem for Linux (WSL). This allows users to run
Linux directly on windows without dual booting or a virtual machine.

Pros:
* Good performance
* Low Risk
* Temporary - you can easily delete everything once you’re done

Cons:
* A little more difficult to set up than a virtual machine

To install Ubuntu via WSL follow the instructions for installing WSL 2 in this tutorial. After this you need to follow the steps outlined below in order to allow Windows to display the GUI’s used in the workshop. Download VcXsrv and run the installer once your download finishes. You can leave the installation configuration as default. Once VcXsrv is installed, run it from the start menu to set it up. Leave "Display settings" and "How to start clients" pages with their default values. For the "Extra settings" page uncheck "Native opengl" and check "Disable access control".

Save your configuration then finish. You should see the VcXsrv logo in the tray at the bottom right of your screen. VcXsrv also requires some .bashrc configuration, so run the following commands in the Ubuntu terminal:

```bash
echo "export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0" >> ~/.bashrc
echo 'export LIBGL_ALWAYS_INDIRECT=0' >> ~/.bashrc
source ~/.bashrc
```

To verify that this process worked, run this in your ubuntu terminal:

```bash
sudo apt update
sudo apt-et install x11-apps
xcalc
```

### VirtualBox

The VirtualBox virtual machine platform can be used to host Ubuntu on a number of different operating systems. This is a type 2 VM, which means there is a layer of software between the guest system and your computer’s hardware. This, as a consequence, limits the performance of the guest system.

Pros:
* Easy to set up
* Low risk
* Temporary - you can easily delete everything once you’re done
* Works on most common operating systems

Cons:
* Mediocre performance

To set up a VirtualBox VM follow this [online tutorial](https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox#1-overview). Similarly to the Hyper-V VM, we recommend that maximal CPU and RAM is provided as soon as the oppurtunity arises. This can be done in VM setup and in Settings -> System -> Processor (navigating from VirtualBox manager). If using this, ensure that you aren’t simultaneously running any other resource-intensive applications.


## Installing ROS 2

Now that you have access to Ubuntu, the next step is to install ROS 2 Galactic. Follow the [offical docs](https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html) on how to do this.

Once you have ROS2 installed, run the following to ensure you always have access to the ROS packages when you open a new terminal.

```bash
echo "source /opt/ros/galactic/setup.bash" >> ~/.bashrc
```

In addition, there are a couple of extra packages we have to install for this Crash Course:

```bash
sudo apt-get install pip3
sudo apt-get install python3-colcon-common-extensions
sudo apt-get install python3-rosdep

sudo rosdep init
rosdep update
```

Congratulations! You should now have ROS 2 installed.