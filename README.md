# EUFS Mobile Robotics Crash Course

This repository contains supporting material for the Mobile Robotics Crash Course organised by [Edinburgh University Formula Student (EUFS)](http://eufs.co/driverless).

EUFS is the leading Formula Student AI team in the UK, having won a class at the [Formula Student UK competition](https://www.imeche.org/events/formula-student/team-information/fs-ai) in 2018, 2018, 2020, 2021 and most recently 2022, where we came away with 5 trophies!

## Introduction
The aim of the Crash Course is to provide a hands-on introduction to the basics of mobile robotics as related to the work done at EUFS for the Formula Student AI competition. However, many of the concepts apply more broadly.

The crash course involves 4 modules:
1. [Introduction to ROS](1.%20Introduction%20to%20ROS/)
2. [Perception](2.%20Perception/)
3. [Sensor Fusion for State Estimation](3.%20Localisation/)
4. [Planning & Control](4.%20Planning%20and%20Control/)

> The Crash Course assumes access to a [School of Informatics DICE machine](https://computing.help.inf.ed.ac.uk/dice-platform) with ROS 2 pre-installed. However, to accommodate those who wish to use their own devices and the wider Formula Student AI community, we include some advice on setting up your own development environment in [0. Environment Setup](0.%20Environment%20Setup/README.md).