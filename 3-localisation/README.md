# Sensor Fusion for State Estimation

Being able to accurately measure the current state of a machine with some level of autonomy is crucial to making it work as desired. These machines can range from everyday cars, with ABS and forward collision warning, to vacuum robots, to the cars we design at EUFS. Measurements such as their position, velocity, and acceleration are necessary for them to be able to make decisions on how to act autonomously.

At EUFS, we are interested in estimates related to the state of the car, such as its pose (meaning position and orientation) and its velocity (both linear and angular). To do this, we collect information from a variety of sensors, and use some of the algorithms and techniques described in this workshop to combine the information they give us to obtain a final, hopefully more accurate, estimate. We do this because each individual sensor is subject to various types of noise, which results in each individual measurement being somewhat off the ‘true’ value. The methods described below allow us to combine information from our sensors in such a way that the resulting estimate is closer to the ‘true’ value.

## Setup

From this point forward, we assume you will run the commands in the `workshop` directory!

To test that your code works after every exercise, you will need a few terminals:

1. A terminal for building the necessary packages. It is good practice to have one terminal dedicated exclusively to this purpose, as per the [ROS Tutorials](https://docs.ros.org/en/galactic/Tutorials/Beginner-Client-Libraries/Creating-Your-First-ROS2-Package.html). To build the packages, use the following command

   ```bash
   colcon build
   ```

2. A terminal for launching the node that you will develop in this workshop. You can run it with:

   ```bash
   ros2 launch launch/launch.py
   ```

3. A terminal to run rqt. This will allow us to plot the velocity to make sure our node is producing sensible outputs. To run rqt use the following command

   ```bash
   rqt
   ```

   A blank window should open. Select `Plugins -> Visualization -> Plot` and type into the topic field `/velocity/twist/twist/linear/x`. The velocity should now appear on the plot. Type `/ground_truth/odom/twist/twist/linear/x` to get the true value.

That's all the set up you need for now!

## Background

You will be writing code exclusively to the `sensor_fusion.py` file inside the `sensor_fusion` package. Boilerplate code is provided that sets up the subscribers and publishers, as well as a few instance variables.

To test your code after every change, you should build the packages as described above, and launch the stack using the provided launch file (remember to source your workspace if you haven't!).

## Exercises

Let’s begin with a very simple scenario. Our car moves only forwards or backwards. We receive velocity commands from control, and measure its acceleration (via an IMU, or Inertial Measurement Unit). We want to fuse information from both of these sources to obtain a better estimate of the velocity.

### Integration

Before we can begin to fuse information from the two sources, we need to convert our acceleration measurements to velocity estimates.

#### Point Integration

The simplest way we can do this is to multiply our acceleration measurement by the time delta since the last acceleration measurement, and add the result to our previous velocity estimate:

$$
\hat{v}_t = \hat{v}_{t - 1} + \delta v = \hat{v}_{t-1} + a_{t} \times \delta t
$$

**Exercise 1**: Write code inside the `callback` function to compute a velocity estimate from the acceleration message received from the IMU.

- Your code should update both the velocity and acceleration of the *state* member variable. We are interested in the x-component of linear acceleration, since that points in the direction of the car's motion
- You will need to look at the [`sensor_msgs/Imu`] message definition (http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Imu.html) to know how to access the acceleration values required.
- You will need to compute the time delta. An attribute is provided for this purpose.
- As a final step, package your estimate in a [`geometry_msgs/Twist`](https://docs.ros.org/en/lunar/api/geometry_msgs/html/msg/Twist.html) message and publish the results using the publisher. For simplicity we are only considering linear velocity along the x-axis!

#### Integrating Noisy Measurements

However, we are disregarding one important quality of all sensor measurements: their accuracy. As one would expect, a velocity estimate’s accuracy will be directly related to the accuracy of the acceleration measurement used to calculate it.

The most common way to represent measurements are as random variables following a [normal (Guassian) distribution](https://en.wikipedia.org/wiki/Normal_distribution). The mean of the distribution will equal the measurement and the variance is directly related to the accuracy of the sensor. You can think of the Gaussian as representing our (or the sensor’s) belief of what the ’true’ value is - the mean being the value we believe is most ’likely’ and the variance telling us how confident we are about our belief.

![](imgs/normal_distribution.png)

Now that we have decided on how to think about uncertainty in our measurements, we need to find out how we can convert our belief of the acceleration measurement (with it’s mean μa,t and variance σa,t) into a belief about the velocity measurement (with it’s mean μv,t and variance σv,t). Fortunately, because the relationship between acceleration and velocity is linear, we can simply multiply the [variance of our acceleration measurement by the square of the time delta](https://en.wikipedia.org/wiki/Variance#Addition_and_multiplication_by_a_constant) to obtain the variance of the velocity delta, which we then add to our previous velocity estimate’s variance

$$
\mu_{v,t} = \mu_{v,t-1} + \Delta v = \mu_{v,t-1} + \mu_{a,t} \times \Delta t
$$

$$
\sigma_{v,t}^2 = \sigma_{v,t-1}^2 + \sigma_{\Delta v}^2 = \sigma_{v,t-1}^2 + {\sigma_{a,t}^2 \times (\Delta t)^2}
$$

**Exercise 2**: Update the code inside the `callback` function to update the covariance of the state. Again, the documentation is your friend!

### Fusion

Now that we know how to calculate velocity estimates from our acceleration measurements, it is time to combine that information with the one obtained from our velocity command.

#### Arithmetic Average

The simplest way to do this is to take an arithmetic average of our estimates. Note that each subscript represents a different source of data, like IMU or a control input.

$$
\hat{v} = \frac{v_1 + v_2}{2}
$$

**Exercise 3**: Modify the code in the callback function to combine the estimate you derived earlier with the velocity measurements. Again have a look at the [geometry_msgs/Twist message definition](http://docs.ros.org/en/melodic/api/geometry_msgs/html/msg/TwistWithCovarianceStamped.html) to find the proper fields.

#### Weighted Averages

However, we are once again disregarding the accuracy of our measurements. It becomes even more important now, since we clearly would like to give more importance to more accurate measurements. We do that by taking a weighted average, where the weight of each estimate is inversely related to its
variance.

$$
\mu_v = \sigma_v^2(\sigma_{v_1}^{-2}\mu_{v_1} + \sigma_{v_2}^{-2}\mu_{v_2})
$$

$$
\sigma_v^2=(\sigma_{v_1}^{-2}+\sigma_{v_2}^{-2})^{-1}
$$

Beware that the twist message does not include a variance for our velocity command! This is a scenario we frequently encounter with new sensors or control inputs in our subteam, and often an educated guess for the accuracy of the sensor is our first starting point. If you're not sure what this should be, try using a variance that is of the same order of magnitude as the acceleration variance (just use a constant value).

**Exercise 4**: Modify your code to:

- Compute a weighted average of the measurements based on their covariance
- Compute the new covariance of the estimate

While this algorithm is fine for simple applications, it is important to remember that a more realistic scenario involves high-frequency, asynchronous data from various sensors, each measuring a different property of the vehicle. Furthermore, we are disregarding the effect of the previous state when calculating the current state, effectively considering them independent values

### Kalman Filter

The [Kalman Filter](https://en.wikipedia.org/wiki/Kalman_filter)j is the most popular sensor fusion algorithm in the field of mobile robotics. It allows us to overcome the shortcomings described above. Indeed, Kalman filters give us [optimal values assuming the errors in our measurements have a Gaussian distribution](https://en.wikipedia.org/wiki/Kalman_filter#Optimality_and_performance). The basic concept of the Kalman Filter is illustrated in Figure 2.

![](imgs/Basic_concept_of_Kalman_filtering.svg.png)

To put it in words, the Kalman filter is a two-step process:

1. **Prediction** - during this step, the previous state and the control commands are used to predict the current state of the vehicle. This is encoded in the following equations:

   $$
   \bar{\mu}_t = A_t\mu_{t-1} + B_tu_t
   $$

   $$
   \bar\Sigma_t = A_t\Sigma_{t-1}A_t^T + R_t
   $$

   - $\mu$ is the state vector. It contains the estimates we produce. In our case, we will be considering the velocity and acceleration of our car.
   - $A$ is the state transition matrix. It encodes the effects of the previous state on the current state.
   - $u$ is the control vector. It contains the commands sent to the car. For simplicity, we won’t be implementing control commands.
   - $B$ encodes the effect of the controls sent to the car on the state vector.
   - $\Sigma$ is the [covariance matrix](https://en.wikipedia.org/wiki/Covariance_matrix).
   - $R$ is the process noise covariance. This is the covariance we add in every prediction step to account for inaccuracies in our model.

   **Exercise 5**: Implement the prediction step of a Kalman filter inside the callback function. It should replace the code from previous exercises.

2. **Update** - during this step, the sensor measurements are used to improve on the estimate obtained in the Prediction step. For each sensor measurement, the following substeps are performed:

   $$
   K_t = \bar\Sigma_tC_t^T(C_t\bar\Sigma_tC_t^T+Q_t)^{-1}
   $$

   $$
   \mu_t=\bar\mu_t+K_t(z_t-C_t\bar\mu_t)
   $$

   $$
   \Sigma_t=(I-K_tC_T)\bar\Sigma_t
   $$

   - $K$ is the Kalman gain of a specific measurement. It represents the weight we give it based on the way it affects the state and its associated covariance.
   - $C$ is the measurement prediction matrix. It encodes the expected measurement produced by a sensor given a state.
   - $Q$ is the measurement covariance matrix. It contains the covariances of the measured values.
   - $z$ is a measurement vector.
   - $I$ is the identity matrix.

   **Exercise 6**: Extend your code inside the `callback` function to perform the update step for both measurements, one after the other.
