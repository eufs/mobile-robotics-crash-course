from rclpy import init, spin, shutdown
from rclpy.node import Node
from rclpy.time import Duration
from rclpy.qos import qos_profile_sensor_data
from message_filters import Subscriber, ApproximateTimeSynchronizer, Cache
from numpy import array, eye
from numpy.linalg import inv
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Twist
from functools import partial

import time

class SensorFusion(Node):

    def __init__(self):

        super().__init__('sensor_fusion')

        self.last_cmd = Twist()
        self.last_imu = Imu()

        self.create_subscription(Twist, 'cmd_vel', self.cmd_callback, 1)
        self.create_subscription(Imu, 'imu/data', self.imu_callback, qos_profile=qos_profile_sensor_data)

        # Initialise velocity publisher
        self.publisher = self.create_publisher(Twist, 'velocity', 1)
        self.create_timer(1, lambda: self.callback(self.last_cmd, self.last_imu))

        self.previous_time = self.get_clock().now()

        # Initial state vector [velocity, acceleration]
        self.state = array([0.0, 0.0])

        # Covariance matrix for the uncertainty in the state
        self.covariance = array([[0.5, 0.0],
                                 [0.0, 0.5]])

        # Covariance matrix to model the uncertainty in the prediction step
        self.process_noise_covariance = array([[1.0, 0.0],
                                               [0.0, 1.0]])

        self.get_logger().info('Initialised sensor fusion node')

    def cmd_callback(self, msg: Twist):
        self.last_cmd = msg

    def imu_callback(self, msg: Imu):
        self.last_imu = msg

    def state_transition_matrix(self, dt: float):
        return

    def control_prediction_matrix(self):
        return

    def imu_prediction_matrix(self):
        return

    def control_noise_covariance(self, wheel_data: Twist):
        return

    def imu_noise_covariance(self, imu_data: Imu):
        return

    def callback(self, speed_data: Twist, imu_data: Imu):

        self.get_logger().info('Entered callback')

        # TODO put you calculations here

        velocity_prediction = Twist()
        # TODO fill in the relevant twist message fields here

        self.publisher.publish(velocity_prediction)


def main(args=None):
    init(args=args)
    sensor_fusion = SensorFusion()
    spin(sensor_fusion)
    shutdown()


if __name__ == '__main__':
    main()
