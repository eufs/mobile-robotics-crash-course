from setuptools import setup

package_name = 'sensor_fusion'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Alexander Alderman Webb',
    maintainer_email='alexanderwebb03@gmail.com',
    description='Localisation Crash Course',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'sensor_fusion = sensor_fusion.sensor_fusion:main'
        ],
    },
)
