import os

from launch.actions import ExecuteProcess, DeclareLaunchArgument
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration

def generate_launch_description():
    bag_name = 'rosbag2_2023_02_27-20_01_51'
    bag_path = os.path.join('data', bag_name)

    return LaunchDescription([
        ExecuteProcess(
            cmd=['ros2', 'bag', 'play', bag_path, '--loop', '--clock'],
            output='screen'
        ),

        Node(
            package='sensor_fusion',
            executable='sensor_fusion',
            name='sensor_fusion',
            output='screen',
            parameters=[{'use_sim_time': LaunchConfiguration('use_sim_time', default='True')}]
        ),    
    ])
