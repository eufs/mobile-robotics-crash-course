# Introduction to ROS 2

The purpose of this module is to introduce the basics of ROS needed for the rest of the [Mobile Robotics Crash Course](https://gitlab.com/eufs/admin/mobile-robotics-crash-course). To do this, we present an exercise covering core ROS concepts such as publishers, subscribers and services. Find the exercise in our [Getting Started](https://gitlab.com/eufs/admin/onboarding/getting_started) repository.

> The Getting Started repository is what we use internally at [Edinburgh University Formula Student](eufs.co/driverless) to onboard new members and introduce them to ROS. We've spent considerable time refining for this purpose. If you find any aspects that could be improved, let us know!

The [ROS 2 Galactic tutorials](https://docs.ros.org/en/galactic/Tutorials.html) is a useful resource that you should keep close at hand throughout this module and the rest of the Crash Course. For those who prefer videos, the [playlist of ROS 2 Tutorials by Hummingbird](https://www.youtube.com/playlist?list=PLgG0XDQqJckkSJDPhXsFU_RIqEh08nG0V) contains video going over some of the same content as the official ROS 2 tutorials.