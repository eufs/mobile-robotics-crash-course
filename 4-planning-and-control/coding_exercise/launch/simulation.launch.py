"""
      This launch file is intended to launch the launcher so it can launch everything for our
      Simulation.
      Cannot record data and does not launch sensor drivers.
"""

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.conditions import LaunchConfigurationNotEquals
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument(
            name="track_name",
            default_value="small_track",
            description="The name of the track"
        ),

        Node(
            name='static_trajectory_publisher',
            package='static_trajectory_publisher',
            executable='static_trajectory_publisher',
            parameters=[{'track_name': LaunchConfiguration('track_name')}],
            condition=LaunchConfigurationNotEquals('track_name', 'none')
        ),

        # Launch the Launcher GUI
        Node(
            name='eufs_launcher',
            package='eufs_launcher',
            executable='eufs_launcher',
            output='screen',
            arguments=['--force-discover'],
            parameters=[
                {'config': f'{get_package_share_directory("eufs_launcher")}/config/eufs_launcher.yaml'},
                {'gui': True}
            ]
        ),
    ])
