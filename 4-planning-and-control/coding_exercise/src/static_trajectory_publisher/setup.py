from setuptools import setup

package_name = 'static_trajectory_publisher'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('share/' + package_name + '/tracks', ['tracks/small_track.csv'])
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Henrich Hegedus',
    maintainer_email='henri.hegedus@gmail.com',
    description='Package for publishing static trajectories',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'static_trajectory_publisher = static_trajectory_publisher.static_trajectory_publisher:main'
        ],
    },
)
