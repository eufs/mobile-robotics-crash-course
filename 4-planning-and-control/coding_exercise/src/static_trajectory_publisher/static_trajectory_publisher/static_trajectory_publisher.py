import rclpy
from rclpy.node import Node
from ament_index_python.packages import get_package_share_directory
import pandas as pd
import numpy as np 

from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener

from eufs_msgs.msg import WaypointArrayStamped, Waypoint
from geometry_msgs.msg import Point

class StaticTrajectoryPublisher(Node):
    def __init__(self, name):
        super().__init__(name)

        track_name = self.declare_parameter("track_name", "small_track").value
        share_dir = get_package_share_directory("static_trajectory_publisher")

        self.track = pd.read_csv(share_dir + "/tracks/" + track_name + ".csv").to_numpy()

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)

        self.trajectory_pub = self.create_publisher(WaypointArrayStamped, "trajectory", 10)
        self.create_timer(0.1, self.publish_track)


    def publish_track(self):
        try:
            t = self.tf_buffer.lookup_transform(
                'base_footprint',
                'map',
                rclpy.time.Time())
        except:
            self.get_logger().info("Failed to lookup transform", throttle_duration_sec=1)
            return
        
        translation = t.transform.translation
        x_t = translation.x
        y_t = translation.y

        q = t.transform.rotation
        _,_, yaw = euler_from_quaternion(q)

        rotation = np.array([
            [np.cos(yaw), -np.sin(yaw)],
            [np.sin(yaw), np.cos(yaw)],
        ])

        translation = np.array([x_t, y_t])
        track = (rotation @ self.track.T).T + translation

        msg = WaypointArrayStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = "base_footprint"
        msg.waypoints = [Waypoint(position = Point(x=x, y=y)) for x, y in track]

        self.trajectory_pub.publish(msg)        

def euler_from_quaternion(q):
    """
    Convert quaternion (w in last place) to euler roll, pitch, yaw
    """
    x = q.x
    y = q.y
    z = q.z
    w = q.w

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = np.arctan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = np.arcsin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = np.arctan2(t3, t4)

    return roll_x, pitch_y, yaw_z


def main():
    rclpy.init(args=None)
    node = StaticTrajectoryPublisher("local_planner")
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
