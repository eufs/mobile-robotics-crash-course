from eufs_msgs.msg import WaypointArrayStamped, Waypoint, ConeArrayWithCovariance, CarState, ConeWithCovariance
from geometry_msgs.msg import Point
from visualization_msgs.msg import Marker
from rclpy.node import Node
import rclpy

from typing import List

import numpy as np
from scipy import interpolate

class Planner(Node):
    def __init__(self, name):
        super().__init__(name)
        # Declare ROS parameters
        self.threshold = self.declare_parameter("threshold", 6.0).value

        # Create subscribers
        self.cones_sub = self.create_subscription(ConeArrayWithCovariance, "/fusion/cones", self.cones_callback, 1)

        # Create publishers
        self.track_line_pub = self.create_publisher(WaypointArrayStamped, "/trajectory", 1)
        self.visualization_pub = self.create_publisher(Marker, "/planner/viz", 1)

    def cones_callback(self, msg):
        blue_cones = self.convert(msg.blue_cones)
        yellow_cones = self.convert(msg.yellow_cones)
        orange_cones = np.concatenate(self.convert(msg.orange_cones), self.convert(msg.big_orange_cones))

        midpoints = self.find_midpoints(blue_cones, yellow_cones, orange_cones)
        midpoints = self.sort_midpoints(midpoints)

        if len(midpoints) == 0:
            return

        # Here we interpolate the path to artificially increase the number of midpoints so that the controllers
        # have more information to work with you don't need to worry about this step
        try:
            tck, _, = interpolate.splprep(np.array([midpoints[0], midpoints[1]]), k=3, s=100)
            midpoints = interpolate.splev(np.linspace(0, 1, 10), tck).T
        except:
            self.get_logger().info("Failed to interpolate")

        self.publish_path(midpoints)
        self.publish_visualisation(midpoints)

    def find_midpoints(self, blue_cones, yellow_cones, orange_cones):
        """
        IMPLEMENT YOURSELF
        Find the midpoints along the track
        :param blue_cones: cone positions
        :param yellow_cones:
        :param orange_cones:
        :return: list of midpoints
        """

        return []

    def sort_midpoints(self, midpoints):
        """
        IMPLEMENT YOURSELF
        Sort the midpoints to so that each consecutive midpoints is further from the car along the path
        :param midpoints:
        :return: sorted midpoints as 2d array where each row is a midpoint
        """

        return []

    def publish_path(self, midpoints):
        waypoint_array = WaypointArrayStamped()
        waypoint_array.header.frame_id = "base_footprint"
        waypoint_array.header.stamp = self.get_clock().now().to_msg()

        for p in midpoints:
            point = Point(x=p[0], y=p[1])
            waypoint = Waypoint(position=point)
            waypoint_array.waypoints.append(waypoint)

        self.track_line_pub.publish(waypoint_array)

    def publish_visualisation(self, midpoints):
        marker = Marker()
        marker.header.frame_id = "base_footprint"
        marker.action = Marker.ADD
        marker.header.stamp = self.get_clock().now().to_msg()
        marker.type = Marker.POINTS
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.id = 0
        marker.scale.x = 0.35
        marker.scale.y = 0.35
        marker.ns = "midpoints"
        for midpoint in midpoints:
            marker.points.append(Point(x=midpoint[0], y=midpoint[1]))

        self.visualization_pub.publish(marker)
    
    def convert(self, cones: List(ConeWithCovariance), struct=''):
        """
        Converts a cone array message into a np array of complex or 2d np array
        :param cones: list of ConeWithCovariance
        :param struct: Type of output list
        :return:
        """
        if struct == "complex":
            return np.array([p.point.x + 1j * p.point.y for p in cones])
        else:
            return np.array([[p.point.x, p.point.y] for p in cones])

def main():
    rclpy.init(args=None)
    node = Planner("local_planner")
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
