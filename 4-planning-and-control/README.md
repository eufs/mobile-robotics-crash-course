# Planning and Control

Planning and control are the last two steps of the autonomous pipeline. We receive information about the state (positions, velocities, orientation) of the car from localization and cone positions from perception. With this information we can create a path for the car to follow and calculate the commands that we need to send to the car for it to follow the path.

This workshop has two parts. The first part is control where you'll make the car follow a preplanned path in the simulation. The second part is planning where you'll learn a bit more about path planning in the context of formula student. You'll also implement a simple path planner that will plan a path for the car to follow. Feel free to complete these parts in whatever order you prefer. 
> **Note:** If you start with the controller please have a look at the Static trajecotry publisher section at the bottom of this document.

## Setup
To begin with, navigate to the `4-planning-and-control/coding_exercise` directory. This is **where we will be running all the commands from**.

Make sure you pull the latest changes in this repository and clone all necessary submodules. Run the following commands.
```
git pull
git submodule update --init --recursive
```

Next, set the following environment variables.
> **NOTE:** Assuming you pulled the MRCC repository to your home directory. If not change `$HOME` to the path to the directory where you cloned MRCC to.

```bash
echo 'export EUFS_MASTER=$HOME/mobile-robotics-crash-course/4-planning-and-control/coding_exercise' >> ~/.bashrc
echo 'export COLCON_DEFAULTS_FILE=$EUFS_MASTER/.colcon/defaults.yaml' >> ~/.bashrc
```
and source the updated `.bashrc`
```bash
source ~/.bashrc
```

Note that this will append lines to your `.bashrc` you migh want to remove them after you complete this workshop.

Lastly build all the necessary software (this will take a while) from the `4-planning-and-control/coding_exercise` directory with 

```bash
colcon build
```

After building completes source the ROS workspace with 
```bash
source $EUFS_MASTER/install/setup.sh
``` 
(you'll have to do this in every new terminal window). You should now be able to run [the simulation](https://gitlab.com/eufs/eufs_sim) (developed by EUFS) by running

```bash
ros2 launch launch/simulation.launch.py
```

When you launch a simulation a GUI should appear. Make sure to select the **publish ground truth TF**. After you've done so you can launch the simulation by pressing the big **Launch!** button.

## Control
To control the car we need to compute the steering angle and acceleration that is needed to follow a path. There are multiple ways of tackling this problem. Today, we will look at a controller named the pure pursuit controller which is very efficient for computing controls when we are in the local planning mode.

Pure pursuit is a geometric path tracking controller. It calculates the steering angle to aim the car at a look ahead point on the reference path. You can think of it as if the car was constantly chasing the look ahead point. The look ahead point has two main goals: regaining the path and maintaining the path. Setting a small look ahead distance will make your car move towards the point quicker which means the car will regain the path quicker but can lead to oscillations as the vehicle can overshoot the path. Setting a larger look ahead makes the control more relaxed which reduces oscillations but can lead to a slow regain of path, especially in corners.

![](imgs/Lookahead.png)

The pure pursuit controller is particularly good for local planning mode because it is very aggressive with a small look ahead distance and therefore it manages to maintain the path exactly. In local planning mode we also drive relatively slow which means that the aggressive controls don’t have a tendency to overshoot the path and cause the vehicle to oscillate.

### Computing Controls

Now a little bit of maths.

![](imgs/Controls%20computation.png)

From the figure above we can compute the radius of the curve

$$\frac{l_d}{\sin(2\alpha)} = \frac{R}{\sin(\frac{\pi}{2}-\alpha)}$$
$$\frac{l_d}{2\sin(\alpha)\cos(\alpha)} = \frac{R}{\cos(\alpha)}$$
$$\frac{l_d}{2\sin(\alpha)} = R$$

Using the dynamic bicycle model we can calculate the steering angle $\sigma$

$$R=\frac{L}{\tan(\sigma)}$$
$$\sigma = \arctan(\frac{2L\sin(\alpha)}{l_d})$$

The steering control does not depend on velocity thus we can change the velocity as we please. We will set the target velocity to depend on the radius $R$ and use a PID controller to maintain this speed.

### Target Speed Calculation

In this workshop, we will only use the curvature of the path to calculate the target speed. More specifically, we will set a maximum lateral acceleration $a_{max}$ that we don’t want to exceed. This will then affect the speed that we send to the car. The signed radius of a path at a certain point is $R = \frac{1}{\kappa}$ where $\kappa$ is the curvature which is the change of the tangent angle $dtan$ over the change in distance $ds$.

$$\kappa = \frac{\,dtan}{\,ds}$$

And at last we will use the formula for centripetal acceleration to calculate the target speed.

$$v = \sqrt{|R|\cdot a_{max}}$$

It might also be a good idea to limit the maximum speed of the car by clipping the velocity above.

### PID

[PID](https://en.wikipedia.org/wiki/PID_controller) stands for proportional-integral-derivative. The controller continuously calculates an error value $e(t)$ as the difference between the current state of the system $y(t)$ and a desired state $r(t)$. To achieve the desired state, the proportional integral and derivative corrections are calculated $u(t)$. In our case the desired state will be the velocity we want to achieve, the current state will be the velocity from localisation and the correction will be the acceleration we apply to the car.

![](imgs/PID.png)

Each of P, I and D terms in the controller serve a different purpose.

* **P**
    Tries to make the difference between $y(t)$ and $r(t)$ equal to 0.

    Only having a P gain can lead to oscillations around the optimum.
* **I**
    Tries to make the integral $\int_0^t e(\tau)d\tau = 0$. This means that the longer the model fails to achieve the target the stronger the signal $u(t)$ will get.

    Eliminates steady state error. In our case, we can imagine steady state error as something stuck in the wheel which slows the car down. Or even air drag at higher speeds can be a cause of steady state error. If this was the case the P term would be constant since it only depends on the difference of $y(t)$ and $r(t)$. However, the I term would grow and the car would accelerate more to achieve the target speed.
* **D**
    The D term tries to keep the change of $e(t) = 0$.

    Effectively dampens the oscillations of the system because it slows down change.

### Coding Exercise

You should now be able to implement a pure pursuit controller from scratch. Don’t worry, we prepared a boilerplate node for you in the `src/control` directory. You will only have to implement the `get_look_ahead_index`, `get_steering`, `get_speed_target` and `get_acceleration` functions.

Since you want to control the acceleration your error $e(t)$ will be the difference between the target speed and your current speed. To then calculate the integral of this error you can take a certain number of previous error and numerically integrate them. The integral part will therefore be
$$
\Sigma_{i=0}^{n} e(t−i) ·dt
$$
where $n$ is the length of the integral window. You are provided with the time delta $dt$ in the `self.period` variable. For the derivative gain you need to figure out how the error has changed from some previous timestep

$$
\frac{e(t)−e(t−1)}{dt}
$$

To test if your implementation works you can simply run the following command in the terminal
```bash
ros2 launch control control.launch.py
```

After you do this the car still won’t move because the simulation is stopping the car from moving. We need to unlock the car in the simulation to allow the car to move. To do this, navigate to the rqt GUI and click on the 'Manual drive' button. If the car hits a few cones and you want to try again just click on 'Reset Simulation' and click on the 'Manual drive' button again.

## Planning

At EUFS we do path planning in two modes. The first we call “local planning", which we use when we don’t know the layout of the track and therefore we are working only with the information we can “see”. The other mode is “global planning", which we deploy when we have mapped the whole track and therefore can optimize for minimum lap time. In this workshop we will offer a brief introduction to local planning.

Problem:
* We have blue cones on the left and yellow cones on the right
* We need to make sure we don’t go outside of the track

Solution:
* Find midpoints between cones of different colors and create a path from them

Since we don’t have the whole track mapped we can only see the color of cones that are in the field of view
of our camera. Using our lidar, we can also see some of the cones outside of the field of view of the camera
but for these cones we don’t know the color.

![](imgs/Planning1.png)

We can connect each pair of different colored cones whose distance is within some threshold and then find
the midpoints of the lines that connect them:

![](imgs/Planning2.png)

The last step is sorting the midpoints so that the point are in the order we want to travel through them.
An optional thing to do is to interpolate the points effectively smoothing out the track.

![](imgs/Planning3.png)

Now we have a local path which we can send to control.

### Exercise

With a brief introduction into how we do local planning in EUFS you can make your own planner whose output path you’ll use to compute the controls for the car and drive it around the track. A boilerplate node is prepared in the planning directory. You will need to implement the `find_midpoints` function and the `sort_midpoints` function.

To run the node, you should run the following in a new terminal

```bash
ros2 launch planning planning.launch.py
```
To test if the planner implementation works you can control the car with the rqt gui and move it around the track to see if the planner is able to find a path. If you managed to implement the controller you can also launch the controller to automate this process (see below).

If you implement the nodes functions correctly the output in the simulation should look something like this:

![](imgs/lcoal_plan.png)

## Launching both planning and control
If you managed to implement both your own planner and controller you can use the two of them to navigate the track. 

By default the simulation also launches a static trajecotry publisher which publishes a preplaned trajectory around the whole circuit. To test the car with your planning you'll want to disable this. All you need to do is relaunch the simulation with the `track_name` parameter set to `"none"`.
```
ros2 launch launch/simulation.launch.py track_name:=none
```

You can now launch both your planning and control node with
```
ros2 launch launch/plan_con.launch.py
```
> **Note:** this is equivalent to launching the two nodes separately


## Possible Improvements

Planning:
1. Think about what you could do when you are only able to see cones of one color. This happens frequently at EUFS. Especially when navigating tight corners such as hairpins.

Control:
1. Instead of a constant look ahead distance for the Pure Pursuit controller, change the distance based on speed. This can help achieve greater stability at high speeds while accurately tracking the path at low speeds.
2. Instead of just looking at the radius at one point to calculate the target speed. You can look at what the car can physically achieve with constraints to its acceleration and traction forces. By propagating those constraints through the whole planed path you can calculate a target speed that should allow the car to make even the tighter turns that are beyond its look ahead distance

## Static trajectory publisher
We've added a planner that publishes the whole trajectory for the small track in simulation. This planner runs by default when you launch the simulation and will allow you to complete the controller before completing the planner.

The planner publishes a set of points around the whole track in the coordinate frame of the vehicle. You can use these points to find the lookahead point for the Pure Pursuit controller.

![](imgs/static_traj_publisher.png)


## Other Reading

1. [High level overview of pure pursuit control for path tracking]()
2. [Lower level overview of pure pursuit and some other controllers (e.g. Stanley controller and MPC)]()
3. [An introductory video to PID controls]()
